#include<stddef.h>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>
#include"mymalloc.h"

//first byte tells us if free or not
//next four bytes stores the size of the block in hexadecimal
static char myblock[4096] = {0, 'f', 'f', 'c'};

int getSize(int address) { //converts myblock array index and returns size of the block
	char blockSize[4];
	for (int i = 0; i < 3; i++) {
		blockSize[i] = myblock[address+i];
	}
	blockSize[3] = '\0';
	int size = (int) strtol(blockSize, NULL, 16);
	return size;
}

void putSize(size_t size, int address) {
	char blockSize[4];
	sprintf(blockSize, "%x", (int)size);
	//printf("putSize: %s\n", blockSize);
	int length = strlen(blockSize);
	int j = 0;
	for (int i = 0; i < 3; i++) {
		if (length < 3 - i) {
			myblock[address+i] = '0';
		} else {
			myblock[address+i] = blockSize[j];
			j++;
		}

		//printf("%c\n", myblock[address+i]);
	}
}

char *mymalloc(size_t size, char* file, int line) {
	
	if (size > 4092) {
		printf("Memory saturation in %s at line %d\n", file, line);
		return NULL;
	}
	
	int currentBlock = 0;
	while (1) {
		int blockSize = getSize(currentBlock+1);
		
		if (myblock[currentBlock] == 0) { //if free	

			if ((int)size <= blockSize) { //if bytes requested fits into block
				myblock[currentBlock] = 1;
				
				if ( (blockSize - (int)size) > 4) { //if theres enough space to split current block and create another
					putSize(size, currentBlock+1);
					int newBlockAddress = currentBlock + 4 + (int)size;
					myblock[newBlockAddress] = 0; //new block is not free
					putSize( blockSize - (int)size - 4, newBlockAddress+1); 
				}
				return (myblock + (currentBlock+4));
			}
		} else { //if not free
			
			currentBlock += blockSize + 4; //currentBlock = nextBlock
			//printf("%d\n", currentBlock);
			if (currentBlock == 4096) { //end of blocks
				printf("No more memory available.\n");
				printf("Memory saturation in %s at line %d\n", file, line);
				return NULL;
			}
			
			
			
		}
	}

	return NULL;	
}

void myfree(void* ptr, char* file, int line) {
	
	if (ptr == NULL) {
		printf("Pointer is null.\n");
		return;
	}

	int previousBlock = -1;
	int currentBlock = 0;
	while (1) {	
		int nextBlock = currentBlock + getSize(currentBlock+1) + 4;
		//printf("%p   %d %d\n", myblock+currentBlock+4, currentBlock, getSize(currentBlock+1));
		if (ptr == (myblock+currentBlock+4)) {
			myblock[currentBlock] = 0; //if pointer addresses match then the block is freed

			if (myblock[nextBlock] == 0) { //if nextBlock is also free, then combine the two blocks
				int newBlockSize = getSize(nextBlock+1) + getSize(currentBlock+1) + 4;
				putSize( (size_t)newBlockSize, currentBlock+1);

			}

			if (previousBlock != -1) { //if the previous block is also free, then combine the two blocks
				if (myblock[previousBlock] == 0) {
					int newBlockSize = getSize(previousBlock+1) + getSize(currentBlock+1) + 4;
					//printf("%d  %d\n", getSize(previousBlock+1), getSize(currentBlock+1));
					putSize( (size_t)newBlockSize, previousBlock+1);
					
				}
			}
			break;

			
		} else {
			previousBlock = currentBlock;
			currentBlock = nextBlock;
			//printf("%d\n", currentBlock);
			if (currentBlock == 4096) { //iterated until the end of memory
				printf("Pointer doesn't exist within memory\n");
				printf("Error in %s at line %d\n", file, line);
				break;
			}
		}
	}

}
