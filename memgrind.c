#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "mymalloc.h"



double time_to_execute_workA = 0;
double time_to_execute_workB = 0;
double time_to_execute_workC = 0;
double time_to_execute_workD = 0;
double time_to_execute_workE = 0;

void workloadA()
{
	clock_t begin = clock() ;
	
	for (int i = 1; i <= 120; i++)
	{
		char * buffer = malloc(1);
		free(buffer);
	}
	
	clock_t after = clock() ;
	time_to_execute_workA += ((double)after - begin) / CLOCKS_PER_SEC;
}

void workloadB()
{
	clock_t begin = clock() ;
	
	char* arr[120];

	for(int i = 0; i < 120; i++)
	{
		arr[i] =  malloc(1);
	}
	
	for(int i = 0; i < 120; i++) free(arr[i]);
	
	clock_t after = clock();
	
	time_to_execute_workB += ((double)after - begin) / CLOCKS_PER_SEC;
}

void workloadC()
{
	clock_t begin = clock() ;
	
	int memory_reserved = 0; //this variable will equal 120 at the end of the for_loop
	char *array[120];
	int malloc_index = 0;//this variable will equal 120 at the end of the for_loop
	int free_index = 0;//this variable will equal 120 at the end of the for_loop
	
	int numbers = 0;//This variable checks if there is pointer to free
	
	srand(time(NULL));
	
	for (int i = 0; i < 240; i++)
	{
	     int x = rand() % 2 ; //generate a random number between 1 and 0
		
		 if(x == 1)
		 {
   	    	
			if(memory_reserved < 120)
			{
				array[malloc_index] = malloc(1);
				malloc_index++;
				memory_reserved ++;
				numbers++;
			}
			else
			{
				free(array[free_index]);
				free_index++;
				numbers--;
			} 
	     } 
	    
   	     else if(x == 0)
		 {
   	    	
   	 	   if(numbers > 0)
		   {
   	 	   	   free(array[free_index]);
			   free_index++;
			   numbers --;
		   }
   	 	   else 
		   {
   	 	   	  array[malloc_index] = malloc(1);
			  malloc_index++;
   	 	   	  memory_reserved ++;
   	 	   	  numbers++;
		   }
   	 	 }	
	}//end of the fro loop
	
	clock_t after = clock();
	
	time_to_execute_workC += ((double)after - begin) / CLOCKS_PER_SEC;
}

void workloadD()
{
	//char*buff = malloc(5000);
	clock_t begin = clock() ;
	
	char*buff [120];
	int x = 0, memory = 0;
	while(x < 120)
	{
		if(x % 2 == 0)
		{
			buff[x] = malloc(sizeof(int));
			memory +=4;
		}
		else 
		{
			buff[x] = malloc(sizeof(char));
			memory ++;
		}
		free(buff[x]);
		x++;
	}
	//printf("Memory value : %d\n", memory);
	
	clock_t after = clock() ;
	time_to_execute_workD += ((double)after - begin) / CLOCKS_PER_SEC;
	
}

void workloadE()
{
	clock_t begin = clock() ;
	
	char *buff [120];
	int x = 1, memory = 0;
	while(x <= 120)
	{
		if(x % 10 != 0)
		{
			//printf("Value of x : %d\n", x);
			buff[x] = malloc(sizeof(int));
			memory += sizeof(int);
		}
		else
		{
			for(int y = x-1; y > (x - 10) ; y--)
			  {
				//printf("Value of y : %d\n", y);
				free(buff[y]);
			  }
		    
		}
		x++;
	}
	//printf("Memory value : %d\n", memory);
	
	clock_t after = clock() ;
	time_to_execute_workE += ((double)after - begin) / CLOCKS_PER_SEC;
}

int main() 
{
	/*
	char *p = malloc(100); //tests
	printf("%p\n", p);
	*/
	
	for(int i = 1; i <= 50; i++)
	{
		workloadA();
		workloadB();
		workloadC();
		workloadD();
		workloadE();				
	}
	
	printf("Mean time WorkloadA : %f second(s).\n", time_to_execute_workA/50);
	printf("Mean time WorkloadB : %f second(s).\n", time_to_execute_workB/50);
	printf("Mean time WorkloadC : %f second(s).\n", time_to_execute_workC/50);
	printf("Mean time WorkloadD : %f second(s).\n", time_to_execute_workD/50);
	printf("Mean time WorkloadE : %f second(s).\n", time_to_execute_workE/50);
    return 0;
}	
